\babel@toc {spanish}{}
\contentsline {chapter}{Agradecimientos}{\es@scroman {i}}
\contentsline {chapter}{Resumen}{\es@scroman {iii}}
\contentsline {chapter}{Abstract}{\es@scroman {v}}
\contentsline {chapter}{Glosario}{\es@scroman {vii}}
\contentsline {chapter}{Cap\IeC {\'\i }tulo 1}{1}
\contentsline {section}{\numberline {}1. Introducci\IeC {\'o}n}{1}
\contentsline {subsection}{\numberline {1.1}Contraparte}{1}
\contentsline {subsubsection}{\numberline {1.1.1}Producto base}{2}
\contentsline {subsection}{\numberline {1.2}Desaf\IeC {\'\i }o}{3}
\contentsline {subsection}{\numberline {1.3}Objetivos}{3}
\contentsline {subsubsection}{\numberline {1.3.1}Enfoque empleado}{4}
\contentsline {subsubsection}{\numberline {1.3.2}Alcances}{4}
\contentsline {subsection}{\numberline {1.4}Proceso del programa de memorias multidisciplinarias}{5}
\contentsline {subsubsection}{\numberline {1.4.1}Integrantes}{5}
\contentsline {subsubsection}{\numberline {1.4.2}Reuniones}{6}
\contentsline {subsubsection}{\numberline {1.4.3}M\IeC {\'e}todos de trabajo}{6}
\contentsline {chapter}{Cap\IeC {\'\i }tulo 2}{9}
\contentsline {section}{\numberline {}2. Estado del arte}{9}
\contentsline {subsection}{\numberline {2.1}Microservicios}{9}
\contentsline {subsection}{\numberline {2.2}Virtualizaci\IeC {\'o}n de los servicios}{10}
\contentsline {subsubsection}{\numberline {2.2.1}M\IeC {\'a}quinas virtuales}{10}
\contentsline {subsubsection}{\numberline {2.2.2}Containers}{11}
\contentsline {subsection}{\numberline {2.3}Orquestador de containers}{11}
\contentsline {subsubsection}{\numberline {2.3.1}Kubernetes}{11}
\contentsline {subsubsection}{\numberline {2.3.2}Helm}{12}
\contentsline {subsubsection}{\numberline {2.3.3}Minikube}{12}
\contentsline {subsection}{\numberline {2.4}DevOps}{13}
\contentsline {subsubsection}{\numberline {2.4.1}Despliegue Canary}{13}
\contentsline {subsection}{\numberline {2.5}Git workflow}{14}
\contentsline {subsubsection}{\numberline {2.5.1}Gitflow o flujo de Git}{14}
\contentsline {subsection}{\numberline {2.6}GitLab}{15}
\contentsline {subsubsection}{\numberline {2.6.1}GitLab CI/CD}{16}
\contentsline {subsection}{\numberline {2.7}Gesti\IeC {\'o}n de configuraci\IeC {\'o}n de software}{16}
\contentsline {subsection}{\numberline {2.8}Resumen comparativo}{17}
\contentsline {chapter}{Cap\IeC {\'\i }tulo 3}{19}
\contentsline {section}{\numberline {}3. Propuesta de desarrollo}{19}
\contentsline {subsection}{\numberline {3.1}Modelo de la arquitectura de la informaci\IeC {\'o}n}{19}
\contentsline {subsection}{\numberline {3.2}Estrategia de implementaci\IeC {\'o}n en el hardware}{21}
\contentsline {subsection}{\numberline {3.3}Reformulaci\IeC {\'o}n de despliegue del software}{23}
\contentsline {subsubsection}{\numberline {3.3.1}Objetivos}{24}
\contentsline {subsubsection}{\numberline {3.3.2}Restricciones}{24}
\contentsline {subsection}{\numberline {3.4}Estrategia DevOps de despliegue}{24}
\contentsline {subsubsection}{\numberline {3.4.1}Ambiente de desarrollo}{25}
\contentsline {subsubsection}{\numberline {3.4.2}Container como m\IeC {\'a}quina virtual}{27}
\contentsline {subsubsection}{\numberline {3.4.3}GitFlow actualizado}{28}
\contentsline {subsubsection}{\numberline {3.4.4}Resumen propuesta}{29}
\contentsline {chapter}{Cap\IeC {\'\i }tulo 4}{33}
\contentsline {section}{\numberline {}4. Resultados}{33}
\contentsline {subsection}{\numberline {4.1}Hitos principales del proyecto}{33}
\contentsline {subsection}{\numberline {4.2}Revisi\IeC {\'o}n de requisitos y dise\IeC {\~n}o}{34}
\contentsline {subsection}{\numberline {4.3}Entorno de soporte y desarrollo}{35}
\contentsline {subsection}{\numberline {4.4}Software resultante}{35}
\contentsline {subsubsection}{\numberline {4.4.1}Sistema de microservicios}{36}
\contentsline {subsubsection}{\numberline {4.4.2}Flujo de CI/CD}{38}
\contentsline {chapter}{Cap\IeC {\'\i }tulo 5}{41}
\contentsline {section}{\numberline {}5. Conclusiones}{41}
\contentsline {subsection}{\numberline {5.1}Trabajos futuros}{42}
\contentsline {chapter}{Ap\'{e}ndice}{45}
\contentsline {section}{\numberline {}6. Ap\IeC {\'e}ndice del Estado del arte}{45}
\contentsline {subsection}{\numberline {6.1}Container}{45}
\contentsline {subsection}{\numberline {6.2}Microservicios}{45}
\contentsline {subsection}{\numberline {6.3}DevOps}{46}
\contentsline {subsubsection}{\numberline {6.3.1}The First Way}{49}
\contentsline {subsubsection}{\numberline {6.3.2}Pipeline de despliegue}{52}
\contentsline {subsubsection}{\numberline {6.3.3}Orquestaci\IeC {\'o}n de servicios}{56}
\contentsline {subsubsection}{\numberline {6.3.4}Pruebas Autom\IeC {\'a}ticas}{58}
